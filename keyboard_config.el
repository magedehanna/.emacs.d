;; keyboared config data


;;C-d duplicate the line 

(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
)
(global-set-key (kbd "C-d") 'duplicate-line)

(global-set-key (kbd "S-C-d") 'delete-char)


;;cursor custimize 
(setq-default cursor-type 'bar)
;;(set-cursor-color "#ffffff")

;; C-c,-v,C-x, for copy pase and cut
;; old C-w cut ,M-w Copy, C-y paste  
(cua-mode t)
(setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
(transient-mark-mode 1) ;; No region when it is not highlighted
(setq cua-keep-region-after-copy t) ;; Standard Windows behaviour


;;(defun copy-word (&optional arg)
;;      "Copy words at point into kill-ring"
;;       (interactive "P")
;;       (copy-thing 'backward-word 'forward-word arg)
;;       ;;(paste-to-mark arg)
;;       )
;;(global-set-key (kbd "C-c w")         (quote copy-word))

(defun backward-copy-word ()
  (interactive)
(save-excursion
  (copy-region-as-kill (point) (progn (backward-word) (point)))))

(global-set-key (kbd "C-w") 'backward-copy-word)

;;C-Z undo  
(global-unset-key "\C-z")
(global-set-key "\C-z" 'advertised-undo)



;;Open terminal
;;(global-set-key (kbd "C-x t") 'shell)
(global-set-key (kbd "C-x t") 'term)

;; fine line number
(global-set-key (kbd "S-C-l") 'goto-line)
;;(global-set-key (kbd "C-l") 'goto-line)

;;zoom in/zoom out
(global-set-key [C-mouse-4] 'text-scale-increase)
(global-set-key [C-mouse-5] 'text-scale-decrease)


;;window resize
(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)


;; No more typing the whole yes or no. Just y or n will do.
(fset 'yes-or-no-p 'y-or-n-p)


;;hide and show h
;;(defvar hs-special-modes-alist
;;  (mapcar 'purecopy
;;  '((c-mode "{" "}" "/[*/]" nil nil)
;;    (c++-mode "{" "}" "/[*/]" nil nil)
;;    (bibtex-mode ("@\\S(*\\(\\s(\\)" 1))
;;    (java-mode "{" "}" "/[*/]" nil nil)
;;    (js-mode "{" "}" "/[*/]" nil))))


(global-set-key (kbd "S-C-f") #'vimish-fold)
(global-set-key (kbd "S-C-u") #'vimish-unfold)
(global-set-key (kbd "S-C-v") #'vimish-fold-delete)


(global-set-key (kbd "C-f") #'helm-swoop)
;;old (global-set-key (kbd "C-f") #'helm-occur)
;;old (global-set-key (kbd "C-f") #'helm-occur)
;;* `M-x helm-swoop` when region active
;;* `M-x helm-swoop` when the cursor is at any symbol
;;* `M-x helm-swoop` when the cursor is not at any symbol
;;* `M-3 M-x helm-swoop` or `C-u 5 M-x helm-swoop` multi separated line culling
;;* `M-x helm-multi-swoop` multi-occur like feature
;;* `M-x helm-multi-swoop-all` apply all buffers
;;* `C-u M-x helm-multi-swoop` apply last selected buffers from the second time
;;* `M-x helm-swoop-same-face-at-point` list lines have the same face at the cursor is on
;;* During isearch `M-i` to hand the word over to helm-swoop
;;* During helm-swoop `M-i` to hand the word over to helm-multi-swoop-all
;;* While doing `helm-swoop` press `C-c C-e` to edit mode, apply changes to original buffer by `C-x C-s`
;;(global-set-key (kbd "M-i") 'helm-swoop)
;;(global-set-key (kbd "M-I") 'helm-swoop-back-to-last-point)
;;(global-set-key (kbd "C-c M-i") 'helm-multi-swoop)
;;(global-set-key (kbd "C-x M-i") 'helm-multi-swoop-all)





;;vimish-fold-unfold
;;vimish-fold-unfold-all
;;vimish-fold-refold
;;vimish-fold-refold-all
;;vimish-fold-delete-all
;;vimish-fold-toggle
;;vimish-fold-toggle-all
