;; enabling scons  

(setq auto-mode-alist
      (cons '("SConstruct" . python-mode) auto-mode-alist))
(setq auto-mode-alist
      (cons '("SConscript" . python-mode) auto-mode-alist))

;;change your Emacs default compile command to scons
;;--Note remove if you wan use make 
(defvar compile-command "scons") 


;;; SCons builds into a 'build' subdir, but we want to find the errors
;;; in the regular source dir.  So we remove build/XXX/YYY/{dbg,final}/ from the
;;; filenames.

;;(defun process-error-filename (filename)
;;  (let ((case-fold-search t))
;;    (setq f (replace-regexp-in-string
;;             "[Ss]?[Bb]uild[\\/].*\\(final\\|dbg\\)[^\\/]*[\\/]" "" filename))
;;    (cond ((file-exists-p f)
;;           f)
;;          (t filename))))

;;(setq compilation-parse-errors-filename-function 'process-error-filename)
