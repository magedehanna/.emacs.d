;;; -*- no-byte-compile: t -*-
(define-package "vimish-fold" "0.2.3" "Fold text like in Vim" '((emacs "24.4") (cl-lib "0.5") (f "0.18.0")))
