;;; -*- no-byte-compile: t -*-
(define-package "helm-gtags" "1.5.6" "GNU GLOBAL helm interface" '((emacs "24.3") (helm "1.7.7")))
