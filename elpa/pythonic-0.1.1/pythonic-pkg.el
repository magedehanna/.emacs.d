;;; -*- no-byte-compile: t -*-
(define-package "pythonic" "0.1.1" "Utility functions for writing pythonic emacs package." '((emacs "24") (cl-lib "0.5") (dash "2.11") (s "1.9") (f "0.17.2")))
