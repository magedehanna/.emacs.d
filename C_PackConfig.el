;;=======================================
;;Code Completion and Navigation
;;=======================================




;;;auto-complete

; initialize package.el
(package-initialize)
; start auto-complete with emacs
(require 'auto-complete)
; do default config for auto-complete
(require 'auto-complete-config)
(ac-config-default)



;;;;;;;;;;;;;;;;;;;;;;;
;;enabel $CEDET
;;;;;;;;;;;;;;;;;;;;;;;

; turn on Semantic
(semantic-mode 1)
; let's define a function which adds semantic as a suggestion backend to auto complete
; and hook this function to c-mode-common-hook
(defun my:add-semantic-to-autocomplete() 
  (add-to-list 'ac-sources 'ac-source-semantic)
)
(add-hook 'c-mode-common-hook 'my:add-semantic-to-autocomplete)
; turn on ede mode 
(global-ede-mode 1)
; create a project for our program.
(ede-cpp-root-project "my project" :file "~/Applications/SOEM-1.3.1/test/linux/new_test/new_test.c"
		      :include-path '("/new_test.h"))

; you can use system-include-path for setting up the system header file locations.
; turn on automatic reparsing of open buffers in semantic
(global-semantic-idle-scheduler-mode 1)

;;;; flaychek 
;;(eval-after-load 'flycheck
;;  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))
;========================================================
;========================================================




;;;;;;;;;;;;;;;;;;;;;;;;;
;;helm 
;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Enable helm-gtags-mode
(add-hook 'c-mode-hook 'helm-gtags-mode)
(add-hook 'c++-mode-hook 'helm-gtags-mode)
(add-hook 'asm-mode-hook 'helm-gtags-mode)

;; customize
(custom-set-variables
 '(helm-gtags-path-style 'relative)
 '(helm-gtags-ignore-case t)
 '(helm-gtags-auto-update t))

;; key bindings
(with-eval-after-load 'helm-gtags
  (define-key helm-gtags-mode-map (kbd "C-x b b") 'helm-buffers-list)
  (define-key helm-gtags-mode-map (kbd "C-x m m") 'helm-M-x)
  (define-key helm-gtags-mode-map (kbd "C-x c c") 'helm-show-kill-ring)
  ;;(define-key helm-gtags-mode-map (kbd "M-t") 'helm-gtags-find-tag)
  ;;(define-key helm-gtags-mode-map (kbd "M-r") 'helm-gtags-find-rtag)
  ;;(define-key helm-gtags-mode-map (kbd "M-s") 'helm-gtags-find-symbol)
  ;;(define-key helm-gtags-mode-map (kbd "M-g M-p") 'helm-gtags-parse-file)
  ;;(define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
  ;;(define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history)
  ;;(define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
  )



;;;;;;;;;;;;;;;;;;;;
;;function-args
;;;;;;;;;;;;;;;;;;;;
;;(fa-config-default)



;;
;;(add-to-list 'company-backends 'company-c-headers)


;;;;;;;;;;;;;;;;;;;;
;;;GDB
;;;;;;;;;;;;;;;;;;;;
;;(setq
 ;; use gdb-many-windows by default
;; gdb-many-windows t

 ;; Non-nil means display source file containing the main routine at startup
 
;; )

;;=======================================
