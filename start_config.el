;;==========================================
;;start up configuration 
;;=========================================

;;line number mode 
(global-linum-mode t)

;; maximize at start up
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;;auto-refresh file 
(global-auto-revert-mode t)


;; Removes *scratch* from buffer after the mode has been set.
(defun remove-scratch-buffer ()
  (if (get-buffer "*scratch*")
      (kill-buffer "*scratch*")))
(add-hook 'after-change-major-mode-hook 'remove-scratch-buffer)



;;https://unix.stackexchange.com/questions/19874/prevent-unwanted-buffers-from-opening
;; Removes *messages* from the buffer.
;;(setq-default message-log-max nil)
;;(kill-buffer "*Messages*")
