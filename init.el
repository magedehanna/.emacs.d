

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(custom-enabled-themes (quote (tango-dark)))
 '(helm-gtags-auto-update t)
 '(helm-gtags-ignore-case t)
 '(helm-gtags-path-style (quote relative))
 '(inhibit-startup-screen t)
 '(package-archives
   (quote
    (("gnu" . "http://elpa.gnu.org/packages/")
     ("MELPA" . "https://stable.melpa.org/packages/"))))
 '(package-selected-packages
   (quote
    (helm-swoop anaconda-mode vimish-fold iedit function-args helm-gtags helm-ebdb flycheck-pycheckers flymake-python-pyflakes flycheck projectile neotree jedi))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;==========================================
;;start up configuration 
;;==========================================
(when window-system
  (load "~/.emacs.d/keyboard_config.el"))

(when window-system
  (load "~/.emacs.d/start_config.el"))


(winner-mode 1)

(setq gud-pdb-command-name "python -m pdb myscript.py")
;;or let you choose the python script
(setq gud-pdb-command-name "python -m pdb ")


;;==========================================


;;==========================================
;;C/C++ configuration 
;;==========================================
(when window-system
  (load "~/.emacs.d/C_PackConfig.el"))

(when window-system
  (load "~/.emacs.d/scons.el"))
;;==========================================


;;==========================================
;;Package configuration 
;;==========================================
;;jedi
;;(add-hook 'python-mode-hook 'jedi:setup)
;;(setq jedi:complete-on-dot t)   


;; Neottree
(add-to-list 'load-path "/some/path/neotree")
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)
(setq neo-smart-open t)

;;projectile
(eval-after-load "projectile"
  '(setq projectile-mode-line
         '(:eval (list " [Pj:"
                       (propertize (projectile-project-name)
                                   'face '(:foreground "#81a2be"))
                       "]"))))

;; flycheck python error 
;;(global-flycheck-mode 1)
;;(with-eval-after-load 'flycheck
;;  (add-hook 'flycheck-mode-hook #'flycheck-pycheckers-setup))

(add-hook 'after-init-hook #'global-flycheck-mode)
;;==========================================
(put 'erase-buffer 'disabled nil)


;;anaconda-mode
(add-hook 'python-mode-hook 'anaconda-mode)
